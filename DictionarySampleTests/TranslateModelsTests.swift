//
//  TranslateModelsTests.swift
//  DictionarySampleTests
//
//  Created by Vlad on 21.11.2021.
//

import XCTest
@testable import DictionarySample

class TranslateModelsTests: XCTestCase {
    func testSearchModel() throws {
        guard let path = Bundle.main.path(forResource: "TestSearchData", ofType: "json") else {
            XCTFail()
            return
        }
        
        let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        let searchData = try JSONDecoder().decode([Word].self, from: jsonData)
        
        XCTAssertEqual(1560, searchData.first?.id)
        XCTAssertEqual("cat", searchData.first?.text)
    }
    
    func testMeaningModel() throws {
        guard let path = Bundle.main.path(forResource: "TestMeaningModel", ofType: "json") else {
            XCTFail()
            return
        }
        
        let jsonData = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
        let meaningData = try JSONDecoder().decode([Meaning].self, from: jsonData)
        
        XCTAssertEqual("65977", meaningData.first?.id)
        XCTAssertEqual("cat", meaningData.first?.text)
    }
}
