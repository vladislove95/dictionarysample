//
//  ViewControllerFabric.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

protocol ViewControllerFabric {
    func instantiateSearchController() -> SearchControllerImpl
    func instantiateDetailController(with word: WordMeaning) -> DetailControllerImpl
}
