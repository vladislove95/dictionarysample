//
//  ViewControllerFabricImpl.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

class ViewControllerFabricImpl: ViewControllerFabric {
    func instantiateSearchController() -> SearchControllerImpl {
        let translationNetworkService = TranslationNetworkServiceImpl(networkService: Storage.networkService)
        let viewModel = SearchViewModelImpl(translationNetworkService: translationNetworkService)
        let controller = SearchControllerImpl(viewModel: viewModel)
       
        viewModel.controller = controller
        
        return controller
    }
    
    func instantiateDetailController(with word: WordMeaning) -> DetailControllerImpl {
        let translationNetworkService = TranslationNetworkServiceImpl(networkService: Storage.networkService)
        let viewModel = DetailViewModelImpl(word: word,
                                            translationNetworkService: translationNetworkService)
        let controller = DetailControllerImpl(viewModel: viewModel)
       
        viewModel.controller = controller
        
        return controller
    }
}
