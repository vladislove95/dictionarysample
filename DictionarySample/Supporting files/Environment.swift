//
//  Environment.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

class Environment {
    static let baseApiURL: URL = URL(string: "https://dictionary.skyeng.ru/api/public/v1/")!
}
