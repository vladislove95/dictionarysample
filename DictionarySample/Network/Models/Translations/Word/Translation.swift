//
//  Translation.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

struct Translation: Decodable {
    let text: String?
    let note: String?
}
