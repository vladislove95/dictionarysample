//
//  Word.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

struct Word: Decodable {
    let id: Int
    let text: String
    let meanings: [WordMeaning]
}
