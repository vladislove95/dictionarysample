//
//  WordMeaning.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

struct WordMeaning: Decodable {
    let id: Int
    let translation: Translation?
    let transcription: String?
}
