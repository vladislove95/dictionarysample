//
//  Image.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

struct Image: Codable {
    enum CodingKeys: String, CodingKey {
        case _url = "url"
    }
    
    let _url: String?
    
    var url: URL? {
        guard let url = _url else {
            return nil
        }
        
        if url.hasPrefix("http://") || url.hasPrefix("https://") {
            return URL(string: url)
        } else {
            return URL(string: "http:\(url)")
        }
   }
}
    
