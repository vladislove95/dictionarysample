//
//  Meaning.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

struct Meaning: Decodable {
    let id: String
    let text: String?
    let transcription: String
    let images: [Image]
    let examples: [MeaningExample]
    let translation: Translation
}
