//
//  NetworkService.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

protocol NetworkService {
    func request<T: Decodable>(endpoint: Endpoint, completion: @escaping NetworkResponseCompletion<T>) -> NetworkRequest?
}
