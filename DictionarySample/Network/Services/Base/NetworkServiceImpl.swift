//
//  NetworkServiceImpl.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation
import UIKit

class NetworkServiceImpl: NetworkService {
    //MARK: -Properties
    private let baseUrl: URL
    
    private lazy var sessionConfiguration: URLSessionConfiguration = {
        let config = URLSessionConfiguration.default
        config.timeoutIntervalForRequest = 15
        
        config.httpAdditionalHeaders = [
            "Content-Type": "application/json"
        ]
        
        return config
    }()
    
    private lazy var session: URLSession = {
        let session = URLSession(configuration: sessionConfiguration)
        return session
    }()
    
    //MARK: -Lifecycle
    init(baseURL: URL) {
        self.baseUrl = baseURL
    }
    
    deinit {
        session.invalidateAndCancel()
    }
}

extension NetworkServiceImpl {
    @discardableResult
    func request<T: Decodable>(endpoint: Endpoint, completion: @escaping NetworkResponseCompletion<T>) -> NetworkRequest? {
        return dataRequest(endpoint: endpoint) { [weak self] response in
            switch response {
            case let .success(data):
                self?.parceDecodable(serverResponse: data, completion: completion)
            case let .failure(error):
                completion(.failure(error))
            }
        }
    }
}

private extension NetworkServiceImpl {
    @discardableResult
    func dataRequest(endpoint: Endpoint, completion: @escaping NetworkDataResponseCompletion) -> NetworkRequest? {
        guard let request = createRequest(for: endpoint) else {
            completion(.failure(.createRequestFail))
            return nil
        }
        
        let dataTask = session.dataTask(with: request) { (data, response, error) in
            guard let data = data, error == nil else {
                completion(.failure(.requestFail(error)))
                return
            }
            
            completion(.success(data))
        }
        
        dataTask.priority = URLSessionTask.highPriority
        dataTask.resume()
        
        return dataTask
    }
    
    func createRequest(for endpoint: Endpoint) -> URLRequest? {
        var params = [String: Any]()
        
        if endpoint.method == .get {
            endpoint.params.forEach({ params[$0] = $1 })
        }
        
        let query = params.map({ (key, value) -> String in
            if let array = value as? [Any] {
                return array.map({ "\(key)=\($0)" }).joined(separator: "&")
            }
            
            return "\(key)=\(value)"
        }).joined(separator: "&").addingPercentEncoding(withAllowedCharacters: .urlHostAllowed)
        
        var path = endpoint.path
        
        if let query = query, !query.isEmpty {
            path += "?\(query)"
        }
        
        guard let url = URL(string: path, relativeTo: baseUrl) else {
            return nil
        }
        
        var request = URLRequest(url: url)
        request.httpMethod = endpoint.method.string
        
        return request
    }
    
    func parceDecodable<T: Decodable>(serverResponse: Data, completion: @escaping NetworkResponseCompletion<T>) {
        do {
            let model = try JSONDecoder().decode(T.self, from: serverResponse)
            completion(.success(model))
        } catch let DecodingError.dataCorrupted(context) {
            let err = NetworkError.decodingFailed("\(context)")
            completion(.failure(err))
        } catch let DecodingError.keyNotFound(key, context) {
            let err = NetworkError.decodingFailed("Key '\(key)' not found. Coding path: \(context.codingPath)")
            completion(.failure(err))
        } catch let DecodingError.valueNotFound(value, context) {
            let err = NetworkError.decodingFailed("Value '\(value)' not found. Coding path: \(context.codingPath)")
            completion(.failure(err))
        } catch let DecodingError.typeMismatch(type, context) {
            let err = NetworkError.decodingFailed("Type '\(type)' mismatch. Coding path: \(context.codingPath)")
            completion(.failure(err))
        } catch {
            let err = NetworkError.decodingFailed(error.localizedDescription)
            completion(.failure(err))
        }
    }
}
