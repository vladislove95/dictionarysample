//
//  TranslationNetworkService.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

protocol TranslationNetworkService {
    func search(searchWord: String, completion: @escaping NetworkResponseCompletion<[Word]>) -> NetworkRequest?
    func detailInfo(id: Int, completion: @escaping NetworkResponseCompletion<[Meaning]>) -> NetworkRequest?
}
