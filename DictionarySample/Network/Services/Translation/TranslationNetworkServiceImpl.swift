//
//  TranslationNetworkServiceImpl.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

class TranslationNetworkServiceImpl: TranslationNetworkService {
    //MARK: - Properties
    private let networkService: NetworkService
    
    //MARK: - Lifecycle
    init(networkService: NetworkService) {
        self.networkService = networkService
    }
}

extension TranslationNetworkServiceImpl {
    @discardableResult
    func search(searchWord: String, completion: @escaping NetworkResponseCompletion<[Word]>) -> NetworkRequest? {
        let endpoint = TranslationEnpoint.search(searchWord)
        return networkService.request(endpoint: endpoint, completion: completion)
    }
    
    @discardableResult
    func detailInfo(id: Int, completion: @escaping NetworkResponseCompletion<[Meaning]>) -> NetworkRequest? {
        let endpoint = TranslationEnpoint.detail(id)
        return networkService.request(endpoint: endpoint, completion: completion)
    }
}
