//
//  NetworkHelpers.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

typealias NetworkResponse<T> = Result<T, NetworkError>

typealias NetworkResponseCompletion<T> = (NetworkResponse<T>) -> Void

typealias NetworkDataResponseCompletion = NetworkResponseCompletion<Data>

typealias NetworkRequest = URLSessionDataTask
