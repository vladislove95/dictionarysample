//
//  NetworkError.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

enum NetworkError: Error {
    case createRequestFail
    case decodingFailed(String)
    case requestFail(Error?)
    case otherError(Error)
}
