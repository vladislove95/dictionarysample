//
//  TranslationEnpoint.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

enum TranslationEnpoint: Endpoint {
    case search(String)
    case detail(Int)
}

extension TranslationEnpoint {
    var path: String {
        switch self {
        case .search:
            return "words/search"
        case .detail:
            return "meanings"
        }
    }
    
    var method: EndpointMethod {
        return .get
    }
    
    var params: [String : Any] {
        var params: [String : Any] = [:]
        
        switch self {
        case let .search(searchWord):
            params["search"] = searchWord
        case let .detail(id):
            params["ids"] = id
        }
        
        return params
    }
}
 
