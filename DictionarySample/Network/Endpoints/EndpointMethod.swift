//
//  EndpointMethod.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

enum EndpointMethod: String {
    case get
    case post
    case put
    case patch
    case delete
}

extension EndpointMethod {
    var string: String {
        return self.rawValue.uppercased()
    }
}
