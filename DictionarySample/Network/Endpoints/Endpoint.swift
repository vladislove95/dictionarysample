//
//  Endpoint.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation
protocol Endpoint {
    //MARK: - Properties
    var path: String { get }
    var method: EndpointMethod { get }
    var params: [String: Any] { get }
}
