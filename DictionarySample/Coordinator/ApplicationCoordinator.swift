//
//  ApplicationCoordinator.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation
import UIKit

class ApplicationCoordinator {
    let fabric: ViewControllerFabric
    let navigationController: UINavigationController
    
    init(navigationController: UINavigationController, fabric: ViewControllerFabric) {
        self.fabric = fabric
        self.navigationController = navigationController
    }
}

//MARK: -Private methods
private extension ApplicationCoordinator {
    func openDetail(for word: WordMeaning) {
        let detailController = fabric.instantiateDetailController(with: word)
        navigationController.pushViewController(detailController, animated: true)
    }
}

//MARK: -Public methods
extension ApplicationCoordinator {
    func start() {
        let searchController = fabric.instantiateSearchController()
        searchController.delegate = self
        navigationController.setViewControllers([searchController], animated: false)
    }
}

//MARK: -SearchControllerDelegate methods
extension ApplicationCoordinator: SearchControllerDelegate {
    func searchController(_ controller: SearchController, didSelectWord word: WordMeaning) {
        openDetail(for: word)
    }
}
