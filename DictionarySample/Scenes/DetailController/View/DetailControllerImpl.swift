//
//  DetailControllerImpl.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation
import UIKit
import Kingfisher

class DetailControllerImpl: ViewController<DetailViewModel>, DetailController {
    //MARK: -Subviews
    private lazy var placeholderView: PlaceholderView = {
        PlaceholderView()
    }()
    
    private lazy var contentContainer: UIView = {
       UIView()
    }()
    
    private lazy var imageView: UIImageView = {
        let image = UIImageView()
        image.contentMode = .scaleAspectFill
        image.layer.cornerRadius = 10
        image.layer.masksToBounds = true
        return image
    }()
    
    private lazy var translationLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 16, weight: .regular)
        label.textColor = .black
        return label
    }()
    
    private lazy var textLabel: UILabel = {
        let label = UILabel()
        label.numberOfLines = 0
        label.font = .systemFont(ofSize: 16, weight: .regular)
        label.textColor = .black
        return label
    }()
    
    private lazy var loader: UIActivityIndicatorView = {
        UIActivityIndicatorView(style: .medium)
    }()
    
    //MARK: -Properties
    weak var delegate: SearchControllerDelegate?
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoadTrigger()
    }
    
    override func setupUI() {
        view.backgroundColor = .white
        
        view.addSubview(placeholderView)
        
        view.addSubview(contentContainer)
        contentContainer.addSubview(imageView)
        contentContainer.addSubview(translationLabel)
        contentContainer.addSubview(textLabel)
        contentContainer.isHidden = true
        
        view.addSubview(loader)
        loader.isHidden = true

        placeholderView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [placeholderView.centerXAnchor.constraint(equalTo: view.centerXAnchor),
             placeholderView.centerYAnchor.constraint(equalTo: view.centerYAnchor),
             placeholderView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6),
             placeholderView.heightAnchor.constraint(lessThanOrEqualTo: view.heightAnchor)])
        
        contentContainer.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [contentContainer.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
             contentContainer.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
             contentContainer.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor),
             contentContainer.bottomAnchor.constraint(lessThanOrEqualTo: view.bottomAnchor)])
        
        imageView.translatesAutoresizingMaskIntoConstraints = false
        let imageViewWidth = imageView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.5)
        imageViewWidth.priority = .defaultLow
        NSLayoutConstraint.activate(
            [imageView.topAnchor.constraint(equalTo: contentContainer.topAnchor, constant: 16),
             imageView.centerXAnchor.constraint(equalTo: contentContainer.centerXAnchor),
             imageViewWidth,
             imageView.widthAnchor.constraint(lessThanOrEqualToConstant: 150),
             imageView.heightAnchor.constraint(equalTo: imageView.widthAnchor, multiplier: 1)])
        
        textLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [textLabel.topAnchor.constraint(equalTo: imageView.bottomAnchor, constant: 16),
             textLabel.leadingAnchor.constraint(equalTo: contentContainer.leadingAnchor, constant: 16),
             textLabel.trailingAnchor.constraint(equalTo: contentContainer.trailingAnchor, constant: -16)])
        
        translationLabel.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [translationLabel.topAnchor.constraint(equalTo: textLabel.bottomAnchor, constant: 16),
             translationLabel.leadingAnchor.constraint(equalTo: contentContainer.leadingAnchor, constant: 16),
             translationLabel.trailingAnchor.constraint(equalTo: contentContainer.trailingAnchor, constant: -16),
             translationLabel.bottomAnchor.constraint(lessThanOrEqualTo: contentContainer.bottomAnchor, constant: -16)])
        
        
        loader.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [loader.centerXAnchor.constraint(equalTo: view.centerXAnchor),
             loader.centerYAnchor.constraint(equalTo: view.centerYAnchor),
             loader.heightAnchor.constraint(equalToConstant: 44),
             loader.widthAnchor.constraint(equalToConstant: 44)])
    }
    
    deinit {
        imageView.kf.cancelDownloadTask()
    }
}

extension DetailControllerImpl {
    func updateNavigationBarTitle(with text: String) {
        self.navigationItem.title = text
    }
    
    func hideShowLoader(show: Bool) {
        DispatchQueue.main.async {
            if show {
                self.loader.startAnimating()
            } else {
                self.loader.stopAnimating()
            }
            self.loader.isHidden = !show
        }
    }
    
    func hideShowPlaceholder(show: Bool, image: UIImage?, title: String?) {
        DispatchQueue.main.async {
            self.placeholderView.image = image
            self.placeholderView.title = title
            self.placeholderView.isHidden = !show
        }
    }
    
    func update(with model: DetailControllerDisplayModel) {
        DispatchQueue.main.async {
            self.contentContainer.isHidden = false
            self.imageView.kf.setImage(with: model.url, placeholder: UIImage(named: "placeholder"))
            self.translationLabel.text = model.translation
            self.textLabel.text = model.text
        }
    }
}
