//
//  DetailController.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation
import UIKit

protocol DetailController: AnyObject {
    func updateNavigationBarTitle(with text: String)
    func hideShowLoader(show: Bool)
    func hideShowPlaceholder(show: Bool, image: UIImage?, title: String?)
    func update(with model: DetailControllerDisplayModel)
}

