//
//  DetailViewModel.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

protocol DetailViewModel {
    func viewDidLoadTrigger()
}
