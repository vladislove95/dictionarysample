//
//  DetailViewModelImpl.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation
import UIKit

class DetailViewModelImpl: DetailViewModel {
    //MARK: -Properties
    weak var controller: DetailController?
    
    private let word: WordMeaning
    private let translationNetworkService: TranslationNetworkService
    
    private var currentRequest: NetworkRequest?
    
    //MARK: -Lifecycle
    init(word: WordMeaning, translationNetworkService: TranslationNetworkService) {
        self.word = word
        self.translationNetworkService = translationNetworkService
    }
    
    deinit {
        currentRequest?.cancel()
    }
}

//MARK: -Input
extension DetailViewModelImpl {
    func viewDidLoadTrigger() {
        controller?.updateNavigationBarTitle(with: "Detail info")
        controller?.hideShowLoader(show: true)

        currentRequest = translationNetworkService.detailInfo(id: word.id) { [weak self] response in
            guard let self = self else {
                return
            }
            
            self.controller?.hideShowLoader(show: false)
            
            switch response {
            case let .success(meanings):
                if let meaning = meanings.first {
                    let model = DetailControllerDisplayModel(url: meaning.images.first?.url,
                                                             text: "Current text: \(meaning.text ?? "")",
                                                             translation: "Translation: \(meaning.translation.text ?? "")")
                    self.controller?.update(with: model)
                } else {
                    self.controller?.hideShowPlaceholder(show: true,
                                                         image: UIImage(named: "not_found"),
                                                         title: "Oups, meaning not found")
                }
            case .failure(_):
                self.controller?.hideShowPlaceholder(show: true,
                                                     image: UIImage(named: "error"),
                                                     title: "Oups, failed to perform current search")
            }
        }
    }
}
