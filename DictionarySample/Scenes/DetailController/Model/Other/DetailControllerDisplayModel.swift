//
//  DetailControllerDisplayModel.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

struct DetailControllerDisplayModel {
    let url: URL?
    let text: String?
    let translation: String?
}
