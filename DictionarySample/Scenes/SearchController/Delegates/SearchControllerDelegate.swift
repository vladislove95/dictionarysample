//
//  SearchControllerDelegate.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

protocol SearchControllerDelegate: AnyObject {
    func searchController(_ controller: SearchController, didSelectWord word: WordMeaning)
}
