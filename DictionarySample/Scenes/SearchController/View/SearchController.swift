//
//  SearchController.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation
import UIKit

protocol SearchController: AnyObject {
    var delegate: SearchControllerDelegate? { get set }
    
    func updateSearchBarPlaceholder(with text: String)
    func updateNavigationBarTitle(with text: String)
    func hideShowLoader(show: Bool)
    func hideShowPlaceholder(show: Bool, image: UIImage?, title: String?)
    func updateSearchResults(words: [WordMeaning])
    func openDetailController(for word: WordMeaning)
}
