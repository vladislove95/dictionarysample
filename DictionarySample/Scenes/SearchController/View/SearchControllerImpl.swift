//
//  SearchControllerImpl.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation
import UIKit

class SearchControllerImpl: ViewController<SearchViewModel>, SearchController {
    //MARK: -Subviews
    private lazy var searchBar: UISearchBar = {
        let search = UISearchBar()
        search.backgroundImage = UIImage()
        return search
    }()
    
    private lazy var tableView: UITableView = {
        let table = UITableView(frame: .zero, style: .plain)
        
        table.tableFooterView = UIView()
        table.keyboardDismissMode = .onDrag
        
        return table
    }()
    
    private lazy var placeholderView: PlaceholderView = {
        PlaceholderView()
    }()
    
    private lazy var loader: UIActivityIndicatorView = {
        UIActivityIndicatorView(style: .medium)
    }()
    
    //MARK: -Properties
    weak var delegate: SearchControllerDelegate?
    
    //MARK: -Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.viewDidLoadTrigger()
    }

    override func setupUI() {
        view.backgroundColor = .white
        
        view.addSubview(searchBar)
        view.addSubview(tableView)
        view.addSubview(placeholderView)
        
        view.addSubview(loader)
        loader.isHidden = true
        
        searchBar.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [searchBar.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
             searchBar.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
             searchBar.topAnchor.constraint(equalTo: view.safeAreaLayoutGuide.topAnchor)])
        
        tableView.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [tableView.leadingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.leadingAnchor),
             tableView.trailingAnchor.constraint(equalTo: view.safeAreaLayoutGuide.trailingAnchor),
             tableView.topAnchor.constraint(equalTo: searchBar.bottomAnchor),
             tableView.bottomAnchor.constraint(equalTo: view.bottomAnchor)])
        
        placeholderView.translatesAutoresizingMaskIntoConstraints = false
        let placeholderViewCenterX = placeholderView.centerXAnchor.constraint(equalTo: tableView.centerXAnchor)
        placeholderViewCenterX.priority = .defaultLow
        let placeholderViewCenterY = placeholderView.centerYAnchor.constraint(equalTo: tableView.centerYAnchor)
        placeholderViewCenterY.priority = .defaultLow
        NSLayoutConstraint.activate(
            [placeholderView.topAnchor.constraint(greaterThanOrEqualTo: tableView.topAnchor, constant: 16),
             placeholderView.bottomAnchor.constraint(lessThanOrEqualTo: view.safeAreaLayoutGuide.bottomAnchor, constant: -16),
             placeholderViewCenterX,
             placeholderViewCenterY,
             placeholderView.widthAnchor.constraint(equalTo: view.widthAnchor, multiplier: 0.6)])
        
        loader.translatesAutoresizingMaskIntoConstraints = false
        NSLayoutConstraint.activate(
            [loader.centerXAnchor.constraint(equalTo: tableView.centerXAnchor),
             loader.centerYAnchor.constraint(equalTo: tableView.centerYAnchor),
             loader.heightAnchor.constraint(equalToConstant: 44),
             loader.widthAnchor.constraint(equalToConstant: 44)])
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        
        tableView.delegate = self
        tableView.dataSource = self
        
        searchBar.delegate = self
        searchBar.searchTextField.delegate = self
    }
    
    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        if let touch = touches.first {
            let currentPoint = touch.location(in: view)
            if tableView.frame.contains(currentPoint) {
                searchBar.resignFirstResponder()
            }
        }
    }
}

//MARK: -SearchController methods
extension SearchControllerImpl {
    func updateSearchBarPlaceholder(with text: String) {
        searchBar.placeholder = text
    }
    
    func updateNavigationBarTitle(with text: String) {
        self.navigationItem.title = text
    }
    
    func hideShowLoader(show: Bool) {
        DispatchQueue.main.async {
            if show {
                self.loader.startAnimating()
            } else {
                self.loader.stopAnimating()
            }
            self.loader.isHidden = !show
        }
    }
    
    func hideShowPlaceholder(show: Bool, image: UIImage?, title: String?) {
        DispatchQueue.main.async {
            self.placeholderView.image = image
            self.placeholderView.title = title
            self.placeholderView.isHidden = !show
        }
    }
    
    func updateSearchResults(words: [WordMeaning]) {
        DispatchQueue.main.async {
            self.tableView.reloadData()
        }
    }
    
    func openDetailController(for word: WordMeaning) {
        delegate?.searchController(self, didSelectWord: word)
    }
}

//MARK: -UITableViewDataSource
extension SearchControllerImpl: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        viewModel.words.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath)
        
        let word = viewModel.words[indexPath.row]
        cell.textLabel?.text = word.translation?.text
        return cell
    }
}

//MARK: -UITableViewDelegate
extension SearchControllerImpl: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.selectWordTrigger(at: indexPath.row)
    }
    
    func tableView(_ tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        return viewModel.headerTitle
    }
}

//MARK: -UISearchBarDelegate methods
extension SearchControllerImpl: UISearchBarDelegate {
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar) {
        viewModel.searchTrigger(searchBar.text)
        searchBar.resignFirstResponder()
    }
    
    func searchBarCancelButtonClicked(_ searchBar: UISearchBar) {
        viewModel.searchTrigger("")
    }
}

//MARK: -UISearchTextFieldDelegate
extension SearchControllerImpl: UISearchTextFieldDelegate {
    func textFieldShouldClear(_ textField: UITextField) -> Bool {
        viewModel.searchTrigger("")
        textField.resignFirstResponder()
        return true
    }
}
