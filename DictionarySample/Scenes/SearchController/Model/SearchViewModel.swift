//
//  SearchViewModel.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

protocol SearchViewModel {
    var words: [WordMeaning] { get }
    var headerTitle: String? { get }
    
    func viewDidLoadTrigger()
    func searchTrigger(_ searchText: String?)
    func selectWordTrigger(at index: Int)
}
