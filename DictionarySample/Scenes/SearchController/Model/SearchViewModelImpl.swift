//
//  SearchViewModelImpl.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation
import UIKit

class SearchViewModelImpl: SearchViewModel {
    //MARK: -Properties
    weak var controller: SearchController?
    
    private let translationNetworkService: TranslationNetworkService
    
    private var currentRequest: NetworkRequest?
    
    var words: [WordMeaning] = [] {
        didSet {
            self.controller?.updateSearchResults(words: words)
        }
    }
    
    var headerTitle: String? = nil
    
    //MARK: -Lifecycle
    init(translationNetworkService: TranslationNetworkService) {
        self.translationNetworkService = translationNetworkService
    }
    
    deinit {
        currentRequest?.cancel()
    }
}

//MARK: -Input
extension SearchViewModelImpl {
    func viewDidLoadTrigger() {
        controller?.updateSearchBarPlaceholder(with: "Type your search here")
        controller?.updateNavigationBarTitle(with: "Translate")
        controller?.hideShowPlaceholder(show: true,
                                        image: UIImage(named: "search"),
                                        title: "Type any word and perform search")
    }
    
    func searchTrigger(_ searchText: String?) {
        headerTitle = nil
        words = []
        
        guard let searchText = searchText, !searchText.isEmpty else {
            controller?.hideShowPlaceholder(show: true,
                                            image: UIImage(named: "search"),
                                            title: "Type any word and perform search")
            return
        }
        
        controller?.hideShowLoader(show: true)
        controller?.hideShowPlaceholder(show: false, image: nil, title: nil)
        
        currentRequest = translationNetworkService.search(searchWord: searchText, completion: { [weak self] response in
            guard let self = self else {
                return
            }
            
            self.controller?.hideShowLoader(show: false)
            
            switch response {
            case let .success(words):
                self.words = words.first?.meanings ?? []
                
                if self.words.isEmpty {
                    self.controller?.hideShowPlaceholder(show: true,
                                                         image: UIImage(named: "not_found"),
                                                         title: "Nothing found, try another search")
                } else {
                    self.headerTitle = "Translations for: \(words.first?.text ?? "")"
                }
            case .failure(_):
                self.controller?.hideShowPlaceholder(show: true,
                                                     image: UIImage(named: "error"),
                                                     title: "Oups, failed to perform current search")
            }
        })
    }
    
    func selectWordTrigger(at index: Int) {
        let word = words[index]
        controller?.openDetailController(for: word)
    }
}
