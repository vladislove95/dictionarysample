//
//  Storage.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation

class Storage {
    static let networkService: NetworkService = NetworkServiceImpl(baseURL: Environment.baseApiURL)
}
