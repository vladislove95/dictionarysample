//
//  ViewController.swift
//  DictionarySample
//
//  Created by Vlad on 21.11.2021.
//

import Foundation
import UIKit

class ViewController<ViewModel>: UIViewController {
    //MARK: -Properties
    let viewModel: ViewModel
    
    //MARK: -Lifecycle
    required init(viewModel: ViewModel) {
        self.viewModel = viewModel
        super.init(nibName: nil, bundle: nil)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupUI()
    }
    
    deinit {
        print(String(describing: self) + "deinited")
    }
    
    //MARK: -Other methods
    func setupUI() {
        
    }
}

